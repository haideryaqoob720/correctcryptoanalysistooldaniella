import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import Toasted from 'vue-toasted'
import VueClipboard from 'vue-clipboard2'
import Cors from 'cors'

const ToastedOptions = {
  position: 'bottom-center',
  duration: 4000,
  theme: 'bubble'
}

Vue.config.productionTip = false

Vue.use(Cors)
Vue.use(VueResource)
Vue.use(Toasted, ToastedOptions)
Vue.use(VueClipboard)

new Vue({
  render: h => h(App),
}).$mount('#app')
